/*Carrousel*/
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {
    slideIndex = 1
  }
  
  if (n < 1) {
    slideIndex = slides.length
  }
  
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  
  slides[slideIndex-1].style.display = "block";  
  
  dots[slideIndex-1].className += " active";
}

/*Auto Complete*/

var tipos = [ 
  { "code": "0", "label": "Mario" }, 
  { "code": "1", "label": "Luigi" }, 
  { "code": "2", "label": "Yoshi" }, 
  { "code": "3", "label": "Peach" }, 
  { "code": "4", "label": "Bowser" }
];

function buscar(){
  var texto = document.body.querySelector("#AutoComplete").value;
  
  var texto_array = texto.split(",").map(function(i){
    return i.trim().toLowerCase();
  });
  
  var resultado = tipos.filter(function(e){
    return ~texto_array.indexOf(e.label.toLowerCase());
  });
  
  var res = document.body.querySelector("#resultado");
  res.innerHTML = '';
  for(var item of resultado){
    res.innerHTML += item.label;
  }
}

const inputEle = document.getElementById('AutoComplete');
inputEle.addEventListener('keyup', function(e){
  var key = e.which || e.keyCode;
  if (key == 13) { 
    buscar()
  }
});