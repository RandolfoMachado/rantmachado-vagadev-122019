$(document).ready(function(){
    
    var idPrincipal = 0;

    var mario;
    var luigi;
    var peach;
    var bowser;
    var yoshi;

    $(".btn-menu").on("click", function(){        
        $(".menu").fadeIn('medium');
    });
    
    $(".btn-close").on("click", function(){
        $(".menu").fadeOut('medium');
    })
    
    $.getJSON("../DadosItems.json", function(datas) {
        mario = datas[0].id;
        luigi = datas[1].id;
        yoshi = datas[2].id;
        peach = datas[3].id;
        bowser = datas[4].id;

        /*Oferta*/
        $("#img1").html('<img src="'+datas[idPrincipal].images[0]['thumb']+'" alt="imagem">');
        $("#img2").html('<img src="'+datas[idPrincipal].images[0]['url']+'" alt="imagem">');
        $("#txt1").html('<h1>'+ datas[idPrincipal].name +'</h1>');
        $("#oferta").html(
                '<p class="priceMain"> de R$ '+ datas[idPrincipal].price +'</p><br />'
            +   '<p class="offerMain"> por <span>R$ '+ datas[idPrincipal].deal +'</span></p>');
        
        $("#botaoOferta").html(
            '<button id="btnCompraAe1" class="abrirModal btnCompraAe">Compra Ae</button>'
        )
        
        
        $("#buscarCep").html(
                '<p>Calcule o Frete</p>'
            +   '<input type='+'text'+' class='+'input1'+' placeholder='+'00000'+' required pattern='+'[0-9]{5}'+'>'
            +   '<input type='+'text'+' class='+'input2'+'  placeholder='+'000'+' required pattern='+'[0-9]{3}'+'>'
            +   '<button type='+'submit'+' class="btnCalcularCep">Calcular</button>');
        
        /*Desc Produto*/
        $("#descProduto").html(
                '<h1 class="h1Alinhamento">Descrição do Produto</h1>'
            +   '<p class="pAlinhamento">'+ datas[mario].description +'</p>');

        /*Compre Junto*/
        $("#promocaoCompreJunto").html(
                '<h1 class="h1AlinhamentoCompreJunto">Compre Junto</h1>'

            +   '<img src="'+datas[mario].images[0]['url']+'" class="imgUmPromoLeveDois" alt="imagem">'
            +   '<p class="h1NameOnePromoLeveDois">Action figure Mario Bros - <br /> Coleção topzeira das galáxias</p>'
            +   '<p class="priceMainOnePromoLeveDois"> de R$ '+ datas[mario].price +'</p>'
            +   '<p class="offerMainOnePromoLeveDois"> por R$ '+ datas[mario].deal +'</p>'

            +   '<i class="fa fa fa-plus fa-lg plusSign"></i>'

            +   '<img src="'+datas[yoshi].images[0]['url']+'" class="imgDoisPromoLeveDois" alt="imagem">'
            +   '<p class="h1NameTwoPromoLeveDois">Action Figures Yoshi - <br /> Coleção topzeira das galáxias</p>'
            +   '<p class="priceMainTwoPromoLeveDois"> de R$ '+ datas[yoshi].price +'</p>'
            +   '<p class="offerMainTwoPromoLeveDois"> por R$ 100,00</p>'

            +   '<i class="fa fa fa-equals fa-lg equalsSign"></i>'
            +   '<div class="box1Azul"><span class="infoValue">Pague somente: </span><br /><span class="infoValue2">'    
            +   'por RS 200,00</span><div class="box2Amarelo"><span class="infoValue3">Economia de: R$ 49,90</span>'
            +   '<button id="btnCompraAe2" class="abrirModal btnCompraAeLeveDois">Compra Ae</button>'
        );

        /*Carroussel*/
        $("#slideShow").append(
            '<h1 class="h1Quemviu">Quem viu, viu também</h1>'
        );

        //Cenario 1 FINZALIDO
        $('#mySlides1').append(
        //item1 
                '<img id="'+mario+'" class="item" src="'+datas[mario].images[0]['url']+'" alt="imagem">'
            +   '<p class="itemName">Action figure Mario Bros - <br /> Coleção topzeira das galáxias</p>'
            +   '<p class="itemPrice"> de R$ '+ datas[mario].price +'</p>'
            +   '<p class="itemDeal"> por R$ '+ datas[mario].deal +'</p>'
        //item2
        +   '<img id="'+peach+'" class="item" src="'+datas[peach].images[0]['url']+'" alt="imagem">'
        +   '<p class="itemName">Action figure Pricess Peach - <br /> Coleção topzeira das galáxias</p>'
        +   '<p class="itemPrice"> de R$ '+ datas[peach].price +'</p>'
        +   '<p class="itemDeal"> por R$ '+ datas[peach].deal +'</p>'
        //item3             
        +   '<img id="'+yoshi+'" class="item" src="'+datas[yoshi].images[0]['url']+'" alt="imagem">'
        +   '<p class="itemName">Action figure Yoshi - <br /> Coleção topzeira das galáxias</p>'
        +   '<p class="itemPrice"> de R$ '+ datas[yoshi].price +'</p>'
        +   '<p class="itemDeal"> por R$ '+ datas[yoshi].deal +'</p>'
        );

        //Cenario 2 FINALIZADO
        $('#mySlides2').append(
        //item1
        '<img id="'+peach+'" class="item" src="'+datas[peach].images[0]['url']+'" alt="imagem">'
        +   '<p class="itemName">Action figure Princess Peach - <br /> Coleção topzeira das galáxias</p>'
        +   '<p class="itemPrice"> de R$ '+ datas[peach].price +'</p>'
        +   '<p class="itemDeal"> por R$ '+ datas[peach].deal +'</p>'
        //item2
        +   '<img id="'+yoshi+'" class="item" src="'+datas[yoshi].images[0]['url']+'" alt="imagem">'
        +   '<p class="itemName">Action figure Yoshi - <br /> Coleção topzeira das galáxias</p>'
        +   '<p class="itemPrice"> de R$ '+ datas[yoshi].price +'</p>'
        +   '<p class="itemDeal"> por R$ '+ datas[yoshi].deal +'</p>'
        //item3
        +   '<img id="'+bowser+'" class="item" src="'+datas[bowser].images[0]['url']+'" alt="imagem">'
        +   '<p class="itemName">Action figure Bowser - <br /> Coleção topzeira das galáxias</p>'
        +   '<p class="itemPrice"> de R$ '+ datas[bowser].price +'</p>'
        +   '<p class="itemDeal"> por R$ '+ datas[bowser].deal +'</p>'
        );

        //Cenario 3
        $('#mySlides3').append(
        //item1
            '<img id="'+yoshi+'" class="item" src="'+datas[yoshi].images[0]['url']+'" alt="imagem">'
            +   '<p class="itemName">Action figure Yoshi - <br /> Coleção topzeira das galáxias</p>'
            +   '<p class="itemPrice"> de R$ '+ datas[yoshi].price +'</p>'
            +   '<p class="itemDeal"> por R$ '+ datas[yoshi].deal +'</p>'
        //item2
        +   '<img id="'+bowser+'" class="item" src="'+datas[bowser].images[0]['url']+'" alt="imagem">'
        +   '<p class="itemName">Action figure Bowser - <br /> Coleção topzeira das galáxias</p>'
        +   '<p class="itemPrice"> de R$ '+ datas[bowser].price +'</p>'
        +   '<p class="itemDeal"> por R$ '+ datas[bowser].deal +'</p>'
        //item3
        +   '<img id="'+mario+'" class="item" src="'+datas[mario].images[0]['url']+'" alt="imagem">'
        +   '<p class="itemName">Action figure Mario Bros - <br /> Coleção topzeira das galáxias</p>'
        +   '<p class="itemPrice"> de R$ '+ datas[mario].price +'</p>'
        +   '<p class="itemDeal"> por R$ '+ datas[mario].deal +'</p>'
        );

        //Cenario 4
        $('#mySlides4').append(
        //item1
        '<img id="'+bowser+'" class="item" src="'+datas[bowser].images[0]['url']+'" alt="imagem">'
        +   '<p class="itemName">Action figure Bowser - <br /> Coleção topzeira das galáxias</p>'
        +   '<p class="itemPrice"> de R$ '+ datas[bowser].price +'</p>'
        +   '<p class="itemDeal"> por R$ '+ datas[bowser].deal +'</p>'
        //itemi2
        +   '<img id="'+mario+'" class="item" src="'+datas[mario].images[0]['url']+'" alt="imagem">'
        +   '<p class="itemName">Action figure Mario Bros - <br /> Coleção topzeira das galáxias</p>'
        +   '<p class="itemPrice"> de R$ '+ datas[mario].price +'</p>'
        +   '<p class="itemDeal"> por R$ '+ datas[mario].deal +'</p>'
        //item3
        +   '<img id="'+peach+'" class="item" src="'+datas[peach].images[0]['url']+'" alt="imagem">'
        +   '<p class="itemName">Action figure Princess Peach - <br /> Coleção topzeira das galáxias</p>'
        +   '<p class="itemPrice"> de R$ '+ datas[peach].price +'</p>'
        +   '<p class="itemDeal"> por R$ '+ datas[peach].deal +'</p>'
        );
        
        /*MODAL*/

        $(".abrirModal").on("click", function(){
            console.log('ACHOU PORRA, RMG IN THA HOUSE');   
            $(".modal").fadeIn('medium');
        });
        
        $(".btn-closeModal").on("click", function(){
            $(".modal").fadeOut('medium');
        })

        /*CountCart*/
        $("#btnCompraAe1").on("click", function(){            
            var count = 0;
            count += 1;
            $('.numero').html(count);
        })

        $("#btnCompraAe2").on("click", function(){
            var count = 0;
            count += 2;
            $('.numero').html(count);
        })

        /*Box Auto Complete*/

        $("#AutoComplete").keydown("click", function(){
            $("#box").fadeIn('fast');
        })

        $("#AutoComplete").blur("click", function(){
            $("#box").fadeOut('fast');
        })

        /*Para quando o campo estiver vazio*/
        
        $("#AutoComplete").keyup("click", function(){
            if($("#AutoComplete").val() == "") {
                $("#box").fadeOut('fast');
            }
        })

        /*BONUS*/
        $("#mySlides1 img").on('click', function(e) {

            idPrincipal = e.currentTarget.id;

            $.ajax({
                method: 'GET',
                url: '../DadosItems.json',
                dataType: 'json'
            }).done(function(){
                $("#img1").html('<img src="'+datas[idPrincipal].images[0]['thumb']+'" alt="imagem">');
                $("#img2").html('<img src="'+datas[idPrincipal].images[0]['url']+'" alt="imagem">');
                $("#txt1").html('<h1>'+ datas[idPrincipal].name +'</h1>');
                $("#oferta").html(
                        '<p class="priceMain"> de R$ '+ datas[idPrincipal].price +'</p><br />'
                    +   '<p class="offerMain"> por <span>R$ '+ datas[idPrincipal].deal +'</span></p>');
            });

        });

        $("#mySlides2 img").on('click', function(e) {

            idPrincipal = e.currentTarget.id;

            $.ajax({
                method: 'GET',
                url: '../DadosItems.json',
                dataType: 'json'
            }).done(function(){
                $("#img1").html('<img src="'+datas[idPrincipal].images[0]['thumb']+'" alt="imagem">');
                $("#img2").html('<img src="'+datas[idPrincipal].images[0]['url']+'" alt="imagem">');
                $("#txt1").html('<h1>'+ datas[idPrincipal].name +'</h1>');
                $("#oferta").html(
                        '<p class="priceMain"> de R$ '+ datas[idPrincipal].price +'</p><br />'
                    +   '<p class="offerMain"> por <span>R$ '+ datas[idPrincipal].deal +'</span></p>');
            });

        });

        $("#mySlides3 img").on('click', function(e) {

            idPrincipal = e.currentTarget.id;

            $.ajax({
                method: 'GET',
                url: '../DadosItems.json',
                dataType: 'json'
            }).done(function(){
                $("#img1").html('<img src="'+datas[idPrincipal].images[0]['thumb']+'" alt="imagem">');
                $("#img2").html('<img src="'+datas[idPrincipal].images[0]['url']+'" alt="imagem">');
                $("#txt1").html('<h1>'+ datas[idPrincipal].name +'</h1>');
                $("#oferta").html(
                        '<p class="priceMain"> de R$ '+ datas[idPrincipal].price +'</p><br />'
                    +   '<p class="offerMain"> por <span>R$ '+ datas[idPrincipal].deal +'</span></p>');
            });

        });

        $("#mySlides4 img").on('click', function(e) {

            idPrincipal = e.currentTarget.id;

            $.ajax({
                method: 'GET',
                url: '../DadosItems.json',
                dataType: 'json'
            }).done(function(){
                $("#img1").html('<img src="'+datas[idPrincipal].images[0]['thumb']+'" alt="imagem">');
                $("#img2").html('<img src="'+datas[idPrincipal].images[0]['url']+'" alt="imagem">');
                $("#txt1").html('<h1>'+ datas[idPrincipal].name +'</h1>');
                $("#oferta").html(
                        '<p class="priceMain"> de R$ '+ datas[idPrincipal].price +'</p><br />'
                    +   '<p class="offerMain"> por <span>R$ '+ datas[idPrincipal].deal +'</span></p>');
            });

        });

    });
});